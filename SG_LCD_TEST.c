//Library https://github.com/fdebrabander/Arduino-LiquidCrystal-I2C-library

#include <LiquidCrystal_I2C.h>
#include <Wire.h>

LiquidCrystal_I2C lcd(0x27, 16, 2); //lcd(hexadecimal address discovered using I2C Scanner code, cols, rows)

void setup() {
    lcd.begin();
    lcd.clear();
    lcd.backlight();
    lcd.setCursor(6,0);
    lcd.print("TEST");
}

void loop() {
    
}
